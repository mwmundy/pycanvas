from setuptools import setup, find_packages

setup(
    name='pycanvas',
    version='0.2.7',
    description='Gather information on all Canvas LMS assignments/modules',
    author='Marshall Mundy',
    author_email='code@mundy.dev',
    url='https://gitlab.com/mwmundy/pycanvas',
    packages=find_packages(),
    install_requires=[
        'aiohttp',
        'aiohttp_retry'
    ],
    license='MIT License',
    entry_points={
        'console_scripts': [
            'canvas_report=pycanvas:cli'
        ]
    },
    zip_safe=False,
    classifiers=[
        'Environment :: Console',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'License :: OSI Approved :: MIT License'
    ]
)
