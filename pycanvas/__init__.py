"""
pycanvas init
"""
import argparse
import asyncio
import os
import json
import logging
import sys

from pathlib import Path
from typing import Dict, List, TextIO

import aiohttp
from aiohttp_retry import RetryClient

from pycanvas.student import Student
from pycanvas.session import CanvasSession

class CanvasParser():
    """
    CanvasParser first determines if the auth token is for a parent or a student.
    If a parent, it will create a list of students that parent observes.  For all
    students, it will pull all course/assignment/quiz/module/submission information
    in order to build 2 HTML reports per student.
    """
    def __init__(self, output_dir: Path):
        """
        Basic init function that sets up two instance variables
        """
        self.index_file = Path(output_dir / "index.html")
        self.session = None
        self.students = []
        self.html_header = """
        <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  </head>
  <body>
        """
        self.html_footer = """
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
        """

    @classmethod
    async def create(cls, base_url: str, token: str, output_dir: Path):
        """
        Async class method to initialize every student
        """
        self = CanvasParser(output_dir)
        connector = aiohttp.TCPConnector(limit=20)
        async with aiohttp.ClientSession(connector=connector) as session:
            retry_session = RetryClient(session)
            self.session = CanvasSession(base_url, token, retry_session)
            students = await self.__get_students()
            for student in students:
                logging.debug(student)
                self.students.append(await Student.create(
                    student_name=student["name"],
                    student_id=student["id"],
                    out_dir=output_dir,
                    session=self.session))
            await retry_session.close()
        return self

    def build_navbar(self):
        navbar_html = """
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <a class="navbar-brand" href="#">pyCanvas</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

  <div class="collapse navbar-collapse" id="navbarContent">
    <ul class="navbar-nav mr-auto">
        """

        for student in self.students:
            navbar_html += f"""<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {student.get_name()}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{student.get_assignments_file()}">Assignments</a>
          <a class="dropdown-item" href="{student.get_modules_file()}">Modules</a>
        </div>
      </li>
            """
        navbar_html += "</ul></div></nav><br>"
        return navbar_html

    async def __get_students(self):
        """
        Determines all of the students it can pull information for.
        """
        # First check to see if you're a parent with observees
        url = "users/self/observees"
        student_info = await self.session.get_json(url)

        if not isinstance(student_info, list) or len(student_info) == 0 or "short_name" not in student_info[0]:
            # Else, you're the student
            url = "users/self"
            student_info = [await self.session.get_json(url)]

        return student_info

    def output_html(self):
        """
        Write the report for each student
        """
        navbar = self.build_navbar()
        with self.index_file.open("w") as index_out:
            index_out.write(self.html_header)
            index_out.write(navbar)
            index_out.write("Select the student above for their pages.")
            index_out.write(self.html_footer)
        logging.info("Index file written to: %s", str(self.index_file))
        for student in self.students:
            student.write_assignment_file(self.html_header, navbar, self.html_footer)
            student.write_module_file(self.html_header, navbar, self.html_footer)


def cli():
    """
    Command-line interface to set up everything required to run the parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--output_dir", type=Path, default=str(Path.home()))
    parser.add_argument("--modules", type=argparse.FileType('w'), default="{}/modules.html".format(str(Path.home())))
    parser.add_argument("--api", default="https://hcpss.instructure.com/api/v1")
    parser.add_argument("--token", default=os.getenv("CANVAS_API_TOKEN"))
    parser.add_argument("-l", "--log", dest="logLevel", default="INFO",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level")
    args = parser.parse_args()

    logging.basicConfig(level=getattr(logging, args.logLevel))

    if args.token is None:
        raise ValueError("API token was not provided")

    if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    canvas = asyncio.run(CanvasParser.create(base_url=args.api, token=args.token, output_dir=args.output_dir))
    canvas.output_html()

if __name__ == "__main__":
    cli()
