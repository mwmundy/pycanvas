"""
Basic class to handle http session management
"""
import logging

from typing import Dict

import aiohttp
from aiohttp_retry import RetryClient

class CanvasSession():
    """
    Handle the json GET requests for all classes across a single session
    """
    def __init__(self, base_url: str, token: str, session: RetryClient):
        """
        Establish the session, request headers, and API base URL
        """
        self.base_url = base_url
        self.headers = {"Authorization": f"Bearer {token}"}
        self.session = session

    async def get_json(self, url_path: str) -> Dict:
        """
        Get the JSON response from the Canvas API
        """
        url = f"{self.base_url}/{url_path}"
        statuses = {x for x in range(100, 600)}
        statuses.remove(200)
        async with self.session.get(url, headers=self.headers, retry_attempts=5, retry_for_statuses=statuses) as response:
            logging.debug("Request %s returned status %d", url, response.status)
            if "X-Rate-Limit-Remaining" in response.headers:
                if float(response.headers["X-Rate-Limit-Remaining"]) < 100:
                    logging.debug("CLOSE TO RATE LIMIT!")
                logging.debug("Rate limit: %s", response.headers["X-Rate-Limit-Remaining"])
            resp_json = []
            if response.status > 199 and response.status < 300:
                resp_json = await response.json()
            elif response.status == 404:
                logging.debug("%s NOT FOUND", url)
            elif response.status == 403:
                logging.error("%s skipped.  Rate limited!", url)
            elif response.status == 401:
                logging.debug("Invalid access token")
                logging.debug(self.headers)

            return resp_json
