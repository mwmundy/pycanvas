"""
Giant (probably too large) class to gather Canvas student information
"""
import asyncio
import logging
import json

from typing import Dict, List
from pathlib import Path

from pycanvas.session import CanvasSession

class Student():
    """
    Giant (probably too large) class to gather Canvas student information
    """
    def __init__(self, student_name: str, student_id: int, out_dir: Path, session: CanvasSession):
        """
        Initialize instance variables
        """
        self.name = student_name
        self.id = student_id
        self.session = session
        self.courses = []
        self.assignments = {}
        self.modules = {}
        self.module_items = {}
        self.quizzes = {}
        self.submissions = {}
        self.assignments_file = Path(out_dir / f"assignments_{student_name.replace(' ', '_')}.html")
        self.modules_file = Path(out_dir / f"modules_{student_name.replace(' ', '_')}.html")

    @classmethod
    async def create(cls, student_name: str, student_id: int, out_dir: Path, session: CanvasSession):
        """
        Async initialization class for a student to gather all course information
        """
        self = Student(student_name, student_id, out_dir, session)
        self.session = session
        self.courses = await self.__get_courses()
        return self

    def get_assignments_file(self):
        return self.assignments_file.name

    def get_modules_file(self):
        return self.modules_file.name

    def get_name(self):
        return self.name

    async def __get_course(self, course:Dict) -> Dict:
        """
        Gather all information for an individual course for this Student
        """
        assignment_task = self.__get_assignments(course["id"])
        module_task = self.__get_modules(course["id"])
        quiz_task = self.__get_quizzes(course["id"])
        assignments, modules, quizzes = await asyncio.gather(assignment_task, module_task, quiz_task)

        self.assignments.update(assignments)
        self.modules.update(modules)
        self.quizzes.update(quizzes)

        submission_tasks = []
        for assignment in assignments[course["id"]]:
            assignment["score"] = None
            submission_tasks.append(self.__get_submissions(course["id"], assignment["id"]))

        submissions = await asyncio.gather(*submission_tasks)
        for submission in submissions:
            self.submissions.update(submission)

        self.assignments.update(assignments)
        self.modules.update(modules)
        self.quizzes.update(quizzes)

        return course

    async def __get_submissions(self, course_id: int, assignment_id: int)->Dict:
        """
        Pull information for a submitted assignment
        """
        url = f"courses/{course_id}/assignments/{assignment_id}/submissions/{self.id}"
        return {assignment_id: await self.session.get_json(url)}

    async def __get_courses(self) -> List[Dict]:
        """
        Get all courses for a Student
        """
        url = f"users/{self.id}/courses?per_page=100"
        all_courses = await self.session.get_json(url)

        course_tasks = []
        for course in all_courses:
            if "name" in course:
                course_tasks.append(self.__get_course(course))

        return await asyncio.gather(*course_tasks)

    async def __get_assignments(self, course_id: int) -> List[Dict]:
        """
        Get all assignments for a Student
        """
        url = f"courses/{course_id}/assignments?per_page=100"
        return {course_id: await self.session.get_json(url)}

    async def __get_quizzes(self, course_id: int) -> Dict:
        """
        Get all quizzes for a Student
        """
        url = f"courses/{course_id}/quizzes?per_page=100"
        return {course_id: await self.session.get_json(url)}

    async def __get_modules(self, course_id: int)->Dict:
        """
        Get all Modules associated with an individual Course
        """
        url = f"courses/{course_id}/modules?per_page=100"
        modules = {course_id: await self.session.get_json(url)}
        module_tasks = []
        for module in modules[course_id]:
            module_tasks.append(self.__get_module_items(course_id, module["id"]))
        items = await asyncio.gather(*module_tasks)
        for item in items:
            self.module_items.update(item)

        return modules

    async def __get_module_items(self, course_id: int, module_id: int)->Dict:
        """
        Get all of the module_items for an individual course module
        """
        url = f"courses/{course_id}/modules/{module_id}/items?per_page=100"
        return {module_id: await self.session.get_json(url)}

    def build_table_header(self, header: str)->str:
        """
        Return the HTML header for an assignment table
        """
        table_header = f'''<h3>{header}</h3>
        <table class="table">
        <thead>
            <tr>
                <th scope="col">Course Name</th>
                <th scope="col">Assignment Name</th>
                <th scope="col">Type</th>
                <th scope="col">Date Due</th>
                <th scope="col">Completed</th>
                <th scope="col">Points Earned</th>
                <th scope="col">Points Possible</th>
            </tr>
        </thead>
        <tbody>\n'''
        return table_header

    def __get_row_color(self, workflow_state: str):
        if workflow_state in ("graded", "submitted"):
            row_class = "table-success"
        elif workflow_state == "unsubmitted":
            row_class = "table-danger"
        else:
            row_class = "table-warning"
        return row_class

    def build_assignment_tables(self)->List[List]:
        """
        This builds lists of HTML strings for submitted and unsubmitted assignments
        """
        submitted_table = []
        unsubmitted_table = []
        unknown_table = []

        for course in self.courses:
            for assignment in self.assignments[course["id"]]:
                workflow_state = self.submissions[assignment["id"]]["workflow_state"]
                row_class = self.__get_row_color(workflow_state)

                row_str = f'<tr class=\"{row_class}\"><td>{course["name"]}</td><td>' + \
                f'<a href=\"{assignment["html_url"]}\">{assignment["name"]}</a></td>' + \
                f'<td>Assignment</td><td>{assignment["due_at"]}</td>' + \
                f'<td>{workflow_state}</td><td>{self.submissions[assignment["id"]].get("score", None)}</td>' + \
                f'<td>{assignment["points_possible"]}</td></tr>\n'

                if workflow_state in ("graded", "submitted"):
                    submitted_table.append(row_str)
                elif workflow_state == "unsubmitted":
                    unsubmitted_table.append(row_str)
                else:
                    unknown_table.append(row_str)


            for quiz in self.quizzes[course["id"]]:
                if not quiz["assignment_id"]:
                    row_class = "table-warning"
                    submitted = "unknown"

                    row_str = f'<tr class=\"{row_class}\"><td>{course["name"]}</td><td>' + \
                    f'<a href=\"{quiz["html_url"]}\">{quiz["title"]}</a></td>' + \
                    f'<td>ungraded {quiz["quiz_type"]}</td><td>{quiz["due_at"]}</td>' + \
                    f'<td>{submitted}</td><td>unknown</td>' + \
                    f'<td>{quiz["points_possible"]}</td></tr>\n'

                    unknown_table.append(row_str)
        return [submitted_table, unsubmitted_table, unknown_table]

    def write_assignment_file(self, html_header: str, navbar: str, html_footer: str):
        """
        Write the assignments HTML file
        """
        [submitted_table, unsubmitted_table, unknown_table] = self.build_assignment_tables()

        with self.assignments_file.open("w") as assign_out:
            assign_out.write(html_header)
            assign_out.write(navbar)

            assign_out.write(self.build_table_header('Unsubmitted Assignments'))
            for line in unsubmitted_table:
                assign_out.write(line)
            assign_out.write("</tbody></table><br>\n")

            assign_out.write(self.build_table_header('Unknown Assignments'))
            for line in unknown_table:
                assign_out.write(line)
            assign_out.write("</tbody></table><br>\n")

            assign_out.write(self.build_table_header('Submitted Assignments'))
            for line in submitted_table:
                assign_out.write(line)
            assign_out.write("</tbody></table><br>\n")

            end_table = """<h6>Row Color Legend</h6>
            <table class="table w-auto">
            <tbody>
                <tr class="small"><td class="table-success small"> </td><td>Completed</td></tr>
                <tr class="small"><td class="table-danger small"> </td><td>Not Complete</td></tr>
                <tr class="small"><td class="table-warning small"> </td><td>Unknown Status</td></tr>
            </tbody>
            </table>
            """
            assign_out.write(end_table)
            assign_out.write(html_footer)
        logging.info("Assignments report written: %s", str(self.assignments_file))

    def write_module_file(self, html_header: str, navbar: str, html_footer: str):
        """
        Write the modules HTML file
        """
        with self.modules_file.open("w") as mod_out:
            mod_out.write(html_header)
            mod_out.write(navbar)
            for course in self.courses:
                mod_out.write("<h4>{}</h4>\n".format(course["name"]))
                table_header = """<table class="table">
            <thead>
                <tr>
                    <th scope="col">Module Name</th>
                    <th scope="col">Module Item</th>
                </tr>
            </thead>
            <tbody>\n"""
                mod_out.write(table_header)

                for module in self.modules[course["id"]]:
                    for module_item in self.module_items[module["id"]]:
                        tr_class = "table-default"
                        if module_item["type"].lower() == "assignment":
                            tr_class = self.__get_row_color(self.submissions[module_item["content_id"]]["workflow_state"])

                        if "html_url" in module_item:
                            mod_out.write("<tr class=\"{}\"><td>{}</td><td><a href=\"{}\">{}</a></td></tr>".format(tr_class, module["name"], module_item["html_url"], module_item["title"]))
                        else:
                            mod_out.write("<tr><td>{}</td><td>{}</td></tr>".format(module["name"], module_item["title"]))
                mod_out.write("</tbody></table>\n\n")
            mod_out.write(html_footer)
        logging.info("Modules report written: %s", str(self.modules_file))
