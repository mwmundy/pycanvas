# pycanvas
This is a proof-of-concept script/Class I'm throwing together to make virtual learning with Canvas less hectic.

Here's what you need to do first:

1. Generate an API token - Go to YOUR_CANVAS_DOMAIN/profile, click "New Access Token" and copy that
2. Set the ENV variable "CANVAS_API_TOKEN to that string
3. Run the script

## To install:
You can download the latest package from the project package repo: https://gitlab.com/mwmundy/pycanvas/-/packages. If, instead, you want to build it yourself use these steps:
1. `python3 setup.py sdist`
2. `pip3 install dist/pycanvas-VERSION.tar.gz`

## Issues with install? Try virtualenv
1. Upgrade pip: `python3 -m pip install --user --upgrade pip`
2. Install virtualenv: `python3 -m pip install --user virtualenv`
3. Set up your virtualenv dir: `python3 -m venv venv3`
4. Activate your env: `source venv3/bin/activate`

After virtualenv is set up and activated, you can pip install whatever you want into this local environment.

## For usage:
`canvas_report -h`

